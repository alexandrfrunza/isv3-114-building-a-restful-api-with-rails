# ISV3-114 Building a RESTful API with Rails
API with random generator of creditscore.

## Installation

Download full archive from [gitlab](https://gitlab.com/alexandrfrunza/isv3-114-building-a-restful-api-with-rails/-/archive/ISV3-114-DEV/isv3-114-building-a-restful-api-with-rails-ISV3-114-DEV.zip) and unzip it to empty folder.

## How to run

Use your terminal and navigate to unziped folder.

```bash
cd path/to/unziped/folder
```

run docker-compose 

```bash
docker-compose up -d
```

create database

```bash
docker-compose exec app rake db:create
```


## Using api


Find in unziped folder file:

```bash
getcreditscore.postman_collection.json
```

Import it in postman.
In that imported file you can find get/post/put/delete instance comands. 